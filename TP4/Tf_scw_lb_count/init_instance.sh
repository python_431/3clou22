#!/usr/bin/env bash

apt -y update && apt -y upgrade
apt -y install apache2

# HACK BIEN POURRI (à virer quand on aura trouvé comment
# fixer les IP privée dans le dhcp du VPC)

myip="$( scw-userdata myip)"

cat > /etc/network/interfaces.d/99-hack<<EOF
auto ens4
iface ens4 inet static
   address $myip
   netmask 255.255.255.0 
   metric 50
   up /sbin/ip route del default via  10.194.203.58
   up /sbin/ip route add default via  10.194.203.58 metric 100
   up /sbin/ip route replace 169.254.42.42/32 via  10.194.203.58
EOF

ifdown -a
ifup -a

touch /root/cloud-init-ok
touch /tmp/cloud-init-ok

