#!/usr/bin/env bash

# NOTE: modifié pour kubernetes

# APT 
apt -y update
#apt -y -o Dpkg::Options::="--force-confnew" upgrade
apt -y install dirmngr
apt -y install git

cat > /etc/apt/sources.list.d/ansible.list <<EOF
deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
EOF
apt-key adv --keyserver keyserver.ubuntu.com \
	    --recv-keys 93C4A3FD7BB9C367

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
cat > /etc/apt/sources.list.d/docker.list <<EOF
deb https://download.docker.com/linux/debian stretch stable
EOF

curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat > /etc/apt/sources.list.d/kubernetes.list << EOF
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF

apt -y update
apt -y install apt-transport-https
apt -y install ansible
apt -y install docker-ce


date > /tmp/cloud-init-ok

