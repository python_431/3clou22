# Projet 3CLOU pour évaluation

Le projet consiste à concervoir un deploiement Terraform mettant
en place totalement automatiquement une application dans le cloud
Scaleway utilisant au moins deux instances et fournissant un service
sur l'Internet.

Scénario proposé : déployer Wordpress avec deux frontaux Web Apache2
(configurations identiques), la base de donnée MariaDB étant sur
une instance dédié et l'accès http/https se faisant sur un load
balanceur (soit une instance dédiée avec NGINX, soit un load
balanceur fourni par Scaleway).

Au total donc : 3 instances + un lb Scaleway ou 4 instances.

Le déploiement Terraform ne doit contenir QUE les ressources
nécessaire à votre déploiement.

Vous pouvez choisir une autre application que Wordpress (Druupal
par exemple) en me prévenant pour que je valide votre choix.

Le déploiemant doit être totalement automatique, je dois pouvoir le
faire moi-même ainsi :

Dans le cas du choix de Wordpress, les grandes étapes sont :

1. Copier mon TP3_vpc/Tf/* chez vous
2. Ajouter dans le plan la définition d'une nouvelle instance pour
   héberger MariaDB
3. À partir du script du TP1 (deploy-wp) fabriquer deux script un
   pour l'initialisation des instances serveurs web (web1 et web2)
4. À partir du script du TP1 (deploy-wp) fabriquer un script qui
   déploye mariadb, le configure et initialise la base wordpress
5. Noter que la conf de wordpress va changer sur un point : l'emplacement
   de la base de donnée (à la place de localhost mettre l'IP de l'instance
   qui héberge mariadb)
5b. MariaDB doit être configuré pour "écouter" sur le réseau 
6. Configurer un load balancer pour envoyer le traffic entrant provenant
   de l'Internet sur les deux serveur web1 et web2 (en http, voire https)
   - méthode 1 : créer un lb "Scaleway" dans le plan Terraform
   - méthode 2 : créer une instance avec nginx correctement configuré
   (selon la méthode il faudra allouer une nouvelle ip publique ou 
    bien rediriger - vpc.tf - le traffic vers l'instance qui fait 
    tourner NGINX)
Note : le point 6 c'est le TP4 (méthode 1 faite, méthode 2 à vous de voir)

Note2 : pour avoir des IP privées fixes pour nos instances (serveurs web et
serveur mariad) vous pouvez reprendre la bidouille du script init_instance.sh.

![](cloudwparchi.png)

~~~~Bash
$ git clone https://....
$ cd .../Projet
$ echo "cold-soup" > teamname.txt
$ make init
$ make apply
~~~~

et aller ensuite sur une url de la forme http://ip_publique/ ; l'ip 
publique étant une des sorties de l'exécution du make apply.

Le projet doit être accompagné d'un README.md contenant :

1. Une description générale de l'architecture déployée
2. La liste de toutes les ressources définies dans les
   fichier .tf indiquant 1) Leur définition (par exemple :
   "une instance est un système Debian GNU/Linux déployé
    dans une machine virtuelle hébergée dans un data center
    de Scaleway" et 2) sont rôle dans l'architecture de l'application
    (par exemple : "exécuter un serveur Web Apache2 où Wordpress
     est installé")


